package models;

import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * Created by HP Elitebook on 3/18/2015.
 */
@Entity
public class StockItem extends Model {
    @Id
    public Long id;
    @ManyToOne
    public ManageStudent managestudent;
    @ManyToOne
    public Student student;
    public Long quantity;
    public static Finder<Long,StockItem> find =new Finder<>(Long.class, StockItem.class);

    public String toString(){
        return String.format("SockItem %d - %d x student %s ",
                id,quantity,student == null ? null :student.id);
    }
}

