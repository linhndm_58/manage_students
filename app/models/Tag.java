package models;

import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.*;
@Entity
public class Tag extends Model {
    @Id
    public Long id;
    @Constraints.Required
    public String name;
    
	@ManyToMany(mappedBy="tags")
    public List<Student> students;
    
    public static Finder<Long,Tag> find =new Finder<>(Long.class, Tag.class);

    public Tag (){
        // Left empty
    }
    public Tag(Long id, String name, Collection<Student> students) {
        this.id = id;
        this.name = name;
        this.students = new LinkedList<Student>(students);
        for (Student student : students) {
            student.tags.add(this);
        }
    }
    public static Tag findById(Long id) {
        return find.byId(id);
    }
}
