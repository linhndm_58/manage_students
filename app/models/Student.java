package models;

import com.avaje.ebean.Page;
import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import play.mvc.PathBindable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import play.db.ebean.Model;



/**
 * Created by HP Elitebook on 3/15/2015.
 */
@Entity
public class Student extends Model implements PathBindable<Student>  {
    @Id
    public Long id;
    @Constraints.Required
    public String login;
    @Constraints.Required
    public String name;
    @Constraints.Required
    public String address;
    @Constraints.Required
    public String email;
    @Constraints.Required
    public String phone;
    @Constraints.Required
    public String course;
    @Constraints.Required
    @Formats.DateTime(pattern = "yyyy-MM-dd")
    public Date date;
    @Constraints.Required
    public String faculty;
	@ManyToMany
    public List<Tag> tags;
    public byte[] picture;
    public static Finder<Long,Student> find =
            new Finder<Long,Student>(Long.class, Student.class);
    @OneToMany(mappedBy="student")
    public List<StockItem> stockItems;
    public static Page<Student> find(int page) {  // trả về trang thay vì List
        return find.where()
                .orderBy("id asc")     // sắp xếp tăng dần theo id
                .findPagingList(5)    // quy định kích thước của trang
                .setFetchAhead(false)  // có cần lấy tất cả dữ liệu một thể?
                .getPage(page);    // lấy trang hiện tại, bắt đầu từ trang 0
    }
    @Override
    public Student bind(String key, String value) {
        return findByLogin(value);
    }

    @Override
    public String unbind(String key) {
        return login;
    }

    @Override
    public String javascriptUnbind() {
        return login;
    }



    public Student(){};

    public Student (String login,String name,String address,String email,String phone
                    ,String faculty,String course,Date date)
    {
        this.login = login;
        this.name = name;
        this.address = address;
        this.email = email;
        this.phone = phone;
        this.faculty = faculty;
        this.course = course;
        this.date = date;
    }
    public String toString()
    {
        return String.format("%s",name);
    }
    private static List<Student> students;
    public static List<Student> findAll()
    {
        return find.all();
    }
    public static Student findByLogin(String login) {
        return find.where().eq("login", login).findUnique();
    }
    public static List<Student> findByName(String tern) {
        final List<Student> results = new ArrayList<Student>();
        for (Student variable : students) {
            if (variable.name.toLowerCase().contains(tern.toLowerCase())) {
                results.add(variable);
            }
        }
        return results;
    }
    public void delete(){
        for (Tag tag : tags){
            tag.students.remove(this);
            tag.save();
        }
        super.delete();

    }

}
