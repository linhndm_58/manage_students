package models;

import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by HP Elitebook on 3/18/2015.
 */
@Entity
public class ManageStudent extends Model {
    @Id
    public Long id;
    public String name;
    @OneToMany(mappedBy="managestudent")
    public List<StockItem> stock = new ArrayList();
    public static Finder<Long,ManageStudent> find =new Finder<>(Long.class, ManageStudent.class);
    public String toString(){
        return name;
    }
}
