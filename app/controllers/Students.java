package controllers;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Page;
import models.*;
import play.data.Form;
import play.mvc.*;
import views.html.students.details;
import views.html.students.list;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by HP Elitebook on 3/14/2015.
 */
@Security.Authenticated(Secured.class)
public class Students extends Controller {
    private static final Form<Student> studentForm = Form.form(Student.class);
    public static Result list(Integer page)
    {
        Page<Student> students = Student.find(page);
        return ok(views.html.catalo.render(students));
    }
    public static Result newStudent()
    {
        return ok(details.render(studentForm));
    }
    public static Result details(Student student)
    {
        if (student == null)
        {
            return notFound(String.format("Student %s does not exist." ,student.login));
        }
        Form<Student> filledForm = studentForm.fill(student);
        return ok(details.render(filledForm));

    }
    public static Result save()
    {
        Form<Student> boundForm = studentForm.bindFromRequest();
        if (boundForm.hasErrors()) {
            flash("error", "Please correct the form below.");
            return badRequest(details.render(boundForm));
        }
        Student student = boundForm.get();
        List<Tag> tags = new ArrayList<Tag>();
        for (Tag tag : student.tags) {
            if (tag.id != null) {
                tags.add(Tag.findById(tag.id));
            }
        }
        student.tags = tags;
        if (student.id==null)
        {
            Ebean.save(student);
        }
        else
        {
            Ebean.update(student);
        }
        flash("success", String.format("Successfully added product %s", student));
        return redirect(routes.Students.list(0));
    }
    public static Result delete(String login) {
        final Student student = Student.findByLogin(login);
        if(student == null) {
            return notFound(String.format("Product %s does not exists.", login));
        }
        student.delete();
        return redirect(routes.Students.list(0));
    }
}
